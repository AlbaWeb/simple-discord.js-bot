# Simple Node.JS Discord Bot

A simple channel assistant and entertainment bot written in Node.js

## Requirements
* server running Node.js v12.16.3 (or higher)
* NPM for installing packages

## Installation
1. Clone the repository to where you want it to run via command line
2. Go to [Discord's Developer Site](https://discord.com/developers/applications) and register your bot application.
3. Paste the security token to the ```.env-example``` and save it as ```.env```
4. Edit the config files in /json-bin/, with all the required information prompted and settings you desire.
5. Once complete use ```npm install``` to install all packages and dependencies.
6. Either add the bot to your service manager or test with ```"node"```  

Any issues or problems will be logged in the console. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)