exports.getList = function() {
    var channelList = [
        {
            name: 'welcome_channel',
            id: '712803209130803203',
        },
        {
            name: 'botlog',
            id: '698345109472935996',
        },
        {
            name: 'radio_apocalypse',
            id: '695269761327562783',
        },
        {
            name: 'general_chat',
            id: '695271116096274463',
        },
        {
            name: 'bottest',
            id: '712804886273851503'
        }
    ];
    return channelList;
}
